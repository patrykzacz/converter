package Controller;
import Model.Conversions;
import javafx.application.Platform;
import View.StringHandler;
import Number.Number;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import java.io.*;
import Number.Type;

public class Controller{

    private Number number = new Number();
    private StringHandler sc = new StringHandler();
    public void buttonBinPressed(TextArea sco,TextArea communicates){


           number.setType(Type.BIN);
           showMessage(communicates,sc.holder.get(0));
           sco.clear();

    }
    public void buttonDecPressed( TextArea sco,TextArea communicates){
            number.setType(Type.DEC);
            showMessage(communicates,sc.holder.get(1));
            sco.clear();

    }
    public void buttonOctPressed(TextArea sco,TextArea communicates){
            number.setType(Type.OCT);
            showMessage(communicates, sc.holder.get(2));
            sco.clear();

    }

    public void buttonHexPressed( TextArea sco, TextArea communicates){
            number.setType(Type.HEX);
            showMessage(communicates,sc.holder.get(3));
            sco.clear();

    }

    public void readerButton(TextField input, Button enter, TextArea communicates, TextArea sco){
        number.setNum(input.getText());
        String numa= number.getNum();
        int i;

        if(enter.isArmed())
        {
            if(number.getType().equals(Type.BIN))
            {
                String tmp = Conversions.binToDec(numa);
                for (i = 0; i < number.getLenght() ; i++)
                {
                    if((numa.charAt(0)!=48 && numa.charAt(0)!= 49 || Integer.parseInt(tmp) > 1000000 )  )showScore(communicates, sc.holder.get(4));
                    else {

                        finalScore(sco,tmp, numa,Conversions.decToHex(tmp),Conversions.decToOct(tmp));

                    };
                }
            }
            if(number.getType().equals(Type.DEC)){
                    for (i = 0; i < number.getLenght() ; i++)
                    {
                        if(number.isNumber(numa) || number.startsWithZero(numa) || number.isInRange(numa)) showScore(communicates,sc.holder.get(4));
                        else{
                            finalScore(sco,numa, Conversions.decToBin(numa),Conversions.decToHex(numa),Conversions.decToOct(numa));
                        }
                    }
                }
            if(number.getType().equals(Type.OCT))
                {   String tmp = Conversions.octToDec(numa);
                    for (i = 0; i < number.getLenght() ; i++)
                    {
                        if(number.isNumber(numa) || number.startsWithZero(numa) || number.isInRange(tmp)) showScore(communicates,sc.holder.get(4));
                        else {
                            finalScore(sco,tmp, Conversions.decToBin(tmp),Conversions.decToHex(tmp),numa);
                        }

                    }
                }
            if(number.getType().equals(Type.HEX))
                {
                    String tmp = Conversions.hexToDec(numa);
                    for (i = 0; i < number.getLenght() ; i++)
                    {
                        if(number.isNumber(numa) && number.isLetter(numa) || number.startsWithZero(numa) || number.isInRange(tmp)) showScore(communicates,sc.holder.get(4));
                        else{
                            finalScore(sco,tmp, Conversions.decToBin(tmp),numa,Conversions.decToOct(tmp));                        };
                    }
                }
            }

        }

    public void showMessage(TextArea communicates, String str){
    communicates.setText(str);
    }


    public void showScore(TextArea sco, String str){
        sco.setText(str);
    }

    public void saveScore(TextArea sco) throws IOException {
        try (PrintStream out = new PrintStream(new FileOutputStream("wynik.txt"))) {
            out.print(sco.getText());
        }
        }


    public void closeWindow(){
        Platform.exit();
    }

    public void finalScore(TextArea sco,String str, String str1,String str2,String str3 ){
        String score= "Liczba w systemie dziesieętnym : " + str + "\n" +
                "Liczba w systemie binarnym : " + str1 + "\n" +
                "Liczba w systemie hexa : " + str2 + "\n" +
                "Liczba w systemie okta : " + str3;
        sco.setText(score);
    }

}
