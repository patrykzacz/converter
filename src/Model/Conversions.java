package Model;

public class Conversions{
    public static String decToBin(String str){
    return Integer.toBinaryString(Integer.parseInt(str));
    }
    public static String decToOct(String str){
        return Integer.toOctalString(Integer.parseInt(str));
    }
    public static String decToHex(String str){
        return Integer.toHexString(Integer.parseInt(str));
    }

    public static String binToDec(String str){
        int num = Integer.parseInt(str,2 );
        str= String.valueOf(num);
        return str;
    };
    public static String hexToDec(String str){
        int num = Integer.parseInt(str,16 );
        str= String.valueOf(num);
      return str;
    };
    public static String octToDec(String str){
        int num = Integer.parseInt(str, 8);
        str=String.valueOf(num);
        return str;
    }

    }


