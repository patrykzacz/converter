package Number;

 public class Number {
    private String num;


    private Type type;
   public Number(){
      this.type=Type.INIT;
    };

    public int getLenght(){ int len= this.num.length();return len; }
    public boolean isNumber(String str){ return str.charAt(0) < 48 || str.charAt(0) > 57; }
    public boolean startsWithZero(String str){ return str.charAt(0)==48; }
    public boolean isLetter(String str){ return (str.charAt(0) < 65 || str.charAt(0) > 70) && (str.charAt(0) < 97 || str.charAt(0) > 102); }
    public void setNum(String num) { this.num = num; }
    public boolean isInRange(String str){

        return (Integer.parseInt(str) > 1000000 || Integer.parseInt(str) < 0 );
    }
    public String getNum() { return num; }
    public Type getType() { return type; }
    public void setType(Type type) { this.type = type; }
}