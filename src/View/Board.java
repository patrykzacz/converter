package View;

import Controller.Controller;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class Board extends Controller {
    private Stage stage = new Stage(StageStyle.UNDECORATED);
    private Button bin = new Button("System Dwójkowy"){};
    private Button dec= new Button("System Dziesiętny");
    private Button oct=new Button("System Ósemkowy");
    private Button hex= new Button("System Szesnastkowy");
    private Button save= new Button("Zapisz");
    private TextField input= new TextField();
    private TextArea sco= new TextArea();
    private TextArea communicates = new TextArea();
    private Button enter= new Button("Wykonaj");
    private Button close= new Button("Wyjdz");
    private Pane root = new Pane();



    public Board()
    {
        generateUI();
    }

    public void generateUI(){
       // stage.setScene(new Scene(root));
        root.setPrefSize(800,600);
        stage.setTitle("Konwerter Liczb");
        stage.setScene(new Scene(root));
        stage.show();
        stage.setResizable(false);
        //dec
        dec.setFont(new Font(15));
        root.getChildren().add(dec);
        dec.setPrefWidth(160);
        dec.setPrefHeight(71);
        dec.setLayoutX(30);
        dec.setLayoutY(180);
        dec.isPressed();
        dec.setOnAction(e->buttonDecPressed(sco,communicates));
        //bin
        bin.setFont(new Font(15));
        root.getChildren().add(bin);
        bin.setPrefWidth(160);
        bin.setPrefHeight(71);
        bin.translateXProperty();
        bin.setLayoutX(30);
        bin.setLayoutY(30);
        bin.setOnAction(e->buttonBinPressed(sco,communicates));
        //oct
        oct.setFont(new Font(15));
        root.getChildren().add(oct);
        oct.setPrefWidth(160);
        oct.setPrefHeight(71);
        oct.setLayoutX(30);
        oct.setLayoutY(330);
        oct.setOnAction(e->buttonOctPressed(sco,communicates));
        //hex
        hex.setFont(new Font(15));
        root.getChildren().add(hex);
        hex.setPrefWidth(160);
        hex.setPrefHeight(71);
        hex.setLayoutX(30);
        hex.setLayoutY(480);
        hex.setOnAction(e->buttonHexPressed(sco,communicates));
        //save
        save.setFont(new Font(15));
        root.getChildren().add(save);
        save.setPrefWidth(126);
        save.setPrefHeight(71);
        save.setLayoutX(350);
        save.setLayoutY(480);
        save.setOnAction(e-> {
            try {
                saveScore(sco);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        //close
        close.setFont(new Font(15));
        root.getChildren().add(close);
        close.setPrefWidth(126);
        close.setPrefHeight(71);
        close.setLayoutX(550);
        close.setLayoutY(480);
        close.setOnAction(e->closeWindow());
        //enter
        enter.setFont(new Font(15));
        root.getChildren().add(enter);
        enter.setPrefWidth(229);
        enter.setPrefHeight(25);
        enter.setLayoutX(400);
        enter.setLayoutY(430);
        enter.setOnAction(e->readerButton(input,enter,communicates,sco));
        //input
        root.getChildren().add(input);
        input.setPrefWidth(550);
        input.setPrefHeight(25);
        input.setLayoutX(250);
        input.setLayoutY(400);
        //sco
        sco.setFont(new Font(15));
        root.getChildren().add(sco);
        sco.setPrefWidth(550);
        sco.setPrefHeight(300);
        sco.setLayoutX(250);
        sco.setLayoutY(80);
        sco.setEditable(false);
        //communicates
        communicates.setFont(new Font(15));
        root.getChildren().add(communicates);
        communicates.setText("Witaj w konwerterze systemów liczbowych! Wybierz System liczbowy z którego \nchcesz konwertować liczbę!");
        communicates.setPrefWidth(550);
        communicates.setPrefHeight(75);
        communicates.setLayoutX(250);
        communicates.setLayoutY(0);
        communicates.setEditable(false);
    }


}
